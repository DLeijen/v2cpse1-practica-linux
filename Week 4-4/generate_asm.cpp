#include <iostream>
#include <fstream>

#include "compressor.hpp" 
#include "decompressor.hpp" 
#include "asm_generator.hpp"

int main( void ){	
   lz_compressor< 4096 > compressor;
   assembly_generator asm_gen;
   
   asm_gen.create_base_asm_file();
 
   std::ifstream f1;
   f1.open( "wilhelmus.txt" );
   if( ! f1.is_open()){
      std::cerr << "input file not opened";
      return -1;      
   }
   
   std::ofstream asm_file;
   asm_file.open(asm_gen.file_name, std::ios::out | std::ios::app);
   
   compressor.compress( 
      [ &f1 ]()-> int { auto c = f1.get(); return f1.eof() ? '\0' : c; },
      [ &asm_file ]( char c ){ 
			if (c == '\n') {
				asm_file << "\\n";
			} else {
				asm_file.put( c );
			}
		}
  );
   
   asm_file <<"\"\n";
   
   f1.close();
   asm_file.close();
}