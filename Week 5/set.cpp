#include "set.hpp"

void Set::add(int i) {
    if (stored_count == int_set.max_size() || contains(i)) {
        return;
    }
    int_set.at(stored_count++) = i;
}

void Set::remove(int i){
    if (stored_count == 0 || !contains(i)) {
        return;
    }
    std::remove(int_set.begin(), int_set.end(), i);
    stored_count -= 1;
}

bool Set::contains(int i){
    if (stored_count == 0) {
        return false;
    }
    auto end = std::next(int_set.begin(), stored_count);
    return std::find(int_set.begin(), end, i) != end;
}

const int Set::inserted_elements(){
	return stored_count;
}

std::ostream& operator<<( std::ostream & os, const Set & set) {
    os << "Integer set: [";
    for (int i = 0; i < set.stored_count; i++) {
        os << set.int_set.at(i);
        if (i != set.stored_count - 1) {
            os << ", ";
        }
    }
    os << "]\n";
    return os;
}