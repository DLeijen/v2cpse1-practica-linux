#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "set.hpp"

TEST_CASE("Adding integers", "[Set::add]") {
	Set s;
	
	REQUIRE(s.inserted_elements() == 0);
	s.add(1);
	REQUIRE(s.inserted_elements() == 1);
	s.add(2);
	REQUIRE(s.inserted_elements() == 2);
	s.add(3);
	REQUIRE(s.inserted_elements() == 3);
	s.add(4);
	REQUIRE(s.inserted_elements() == 4);
	s.add(1);
	REQUIRE(s.inserted_elements() == 4);
	s.add(5);
	REQUIRE(s.inserted_elements() == 5);
	s.add(6);
	REQUIRE(s.inserted_elements() == 6);
	s.add(7);
	REQUIRE(s.inserted_elements() == 7);
	s.add(8);
	REQUIRE(s.inserted_elements() == 8);
	s.add(9);
	REQUIRE(s.inserted_elements() == 9);
	s.add(10);
	REQUIRE(s.inserted_elements() == 10);
	s.add(11);
	REQUIRE(s.inserted_elements() == 10);
}

TEST_CASE("Removing integers", "[Set::remove]") {
	Set s;
	
	s.remove(0);
	REQUIRE(s.inserted_elements() == 0);
	
	s.add(1);
	s.add(2);
	s.add(3);
	s.add(4);
	s.add(5);
	s.add(6);
	s.add(7);
	s.add(8);
	s.add(9);
	s.add(10);
	
	s.remove(10);
	REQUIRE(s.inserted_elements() == 9);
	
	s.remove(9);
	REQUIRE(s.inserted_elements() == 8);
	
	
	s.remove(9);
	REQUIRE(s.inserted_elements() == 8);
	
	s.remove(1);
	REQUIRE(s.inserted_elements() == 7);
	
	s.remove(-1);
	REQUIRE(s.inserted_elements() == 7);
}

TEST_CASE("Contains integers", "[Set::contains]") {
	Set s;
	
	s.add(10);
	
	REQUIRE(s.contains(10));
	REQUIRE(!s.contains(11));
	s.remove(10);
	REQUIRE(!s.contains(10));
	s.add(10);
	REQUIRE(s.contains(10));
}