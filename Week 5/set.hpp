#ifndef _SET_HPP_
#define _SET_HPP_

#include <algorithm> 
#include <array>
#include <iostream>

class Set {
private:
	 std::array<int, 10> int_set;
	 int stored_count = 0;
public:
    void add(int);
    void remove(int);
    bool contains(int);
	const int inserted_elements();
    friend std::ostream& operator<<( std::ostream & os, const Set & set);
};

#endif