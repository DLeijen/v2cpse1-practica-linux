#include "set.hpp"

#include <iostream>

int main() {
    Set s;
    s.add(1);
    s.add(5);
    std::cout << s;
    std::cout << "The set contains 10: " << s.contains(10) << "\n";
    s.add(10);
    std::cout << "The set contains 10: " << s.contains(10) << "\n";
    s.remove(1);
    std::cout << "The set contains 1: " << s.contains(1) << "\n";
}