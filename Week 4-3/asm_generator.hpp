#ifndef _ASM_GENERATOR_HPP
#define _ASM_GENERATOR_HPP

#include <iostream>
#include <fstream>

class assembly_generator {
private:
	const std::string cpu = ".cpu cortex-m0\n\n";
	const std::string double_indent = "\t\t";
	const std::string text = ".text\n";
	const std::string str_var = "str:\t.asciz\t\"";
	
	std::ofstream asm_file;
	
public:
	const std::string file_name = "compressed.asm";
	
	void create_base_asm_file () {
	   asm_file.open( file_name, std::ios::out | std::ios::trunc);
	   if( ! asm_file.is_open()){
		  std::cerr << "asm file not opened";
		  return;      
	   }
	   asm_file << cpu << text << str_var;
	   asm_file.close();
	}
	
	const std::string global_dec = ".global str\n";
};

#endif