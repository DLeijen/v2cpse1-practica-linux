.cpu cortex-m0

.global print_asciz

print_asciz:
	push { lr, r0, r4 }
	mov r4, r0 				// move arg1 to r4
loop:
	ldrb r0, [r4] 				// load byte mem value from r4 into r0
	add r0, r0, #0 			// If r0 == 0
	beq done					// goto done
	bl uart_put_char 		// print char
	add r4, r4, #1 			//  increment pointer
	b loop
done:
	pop { pc, r0, r4 }
