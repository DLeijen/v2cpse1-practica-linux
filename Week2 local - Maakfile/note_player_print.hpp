#ifndef _NOTE_PLAYER_PRINT_HPP
#define _NOTE_PLAYER_PRINT_HPP

#include "note_player.hpp"

#include <iostream>

// ===========================================================================
//
// simple note player that prints to console
//
// ===========================================================================

class note_player_print : public note_player {
public: 
   note_player_print( ){}
   
   void play( const note & n ) override;
};

#endif
