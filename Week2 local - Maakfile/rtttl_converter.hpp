#include "note.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <ios>
#include <iostream>
#include <fstream>

// ===========================================================================
//
// rtttl string player interface
//
// ===========================================================================

void rtttl_generate_melody( const char *s );
std::string determineNote(const int frequency);
std::string determineDuration(const int duration);