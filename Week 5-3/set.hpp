#ifndef _SET_HPP_
#define _SET_HPP_

#include <algorithm> 
#include <array>
#include <iostream>
#include <boost/type_index.hpp>

template <typename T, int count>
class Set {
private:
	 std::array<T, count> storage_set;
	 int stored_count = 0;
public:
    void add(T t){
		if (stored_count == storage_set.max_size() || contains(t)) {
			return;
		}
		storage_set.at(stored_count++) = t;
	}
	
    void remove(T t){
		if (stored_count == 0 || !contains(t)) {
			return;
		}
		std::remove(storage_set.begin(), storage_set.end(), t);
		stored_count -= 1;
	}

    bool contains(T t){
		if (stored_count == 0) {
			return false;
		}
		auto end = std::next(storage_set.begin(), stored_count);
		return std::find(storage_set.begin(), end, t) != end;
	}
	
	const int inserted_elements(){
		return stored_count;
	}
	
	T max() {
		if (stored_count == 0) {
			throw "Set is empty!";
		}
		
		T max;
		for (int i = 0; i < stored_count; i++) {
			max = i == 0 || max < storage_set.at(i) ? storage_set.at(i) : max;
		}
		return max;
	}

    friend std::ostream& operator<<( std::ostream & os, const Set<T, count> & set){
		os << /*boost::typeindex::typeid<T>().pretty_name() <<*/ "set: [";
		for (int i = 0; i < set.stored_count; i++) {
			os << set.storage_set.at(i);
			if (i != set.stored_count - 1) {
				os << ", ";
			}
		}
		os << "]\n";
		return os;
	}
};

#endif