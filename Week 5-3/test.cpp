#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "set.hpp"

TEST_CASE("Adding integers", "[Set<int, 10>::add]") {
	Set <int, 10> s;
	
	REQUIRE(s.inserted_elements() == 0);
	s.add(1);
	REQUIRE(s.inserted_elements() == 1);
	s.add(2);
	REQUIRE(s.inserted_elements() == 2);
	s.add(3);
	REQUIRE(s.inserted_elements() == 3);
	s.add(4);
	REQUIRE(s.inserted_elements() == 4);
	s.add(1);
	REQUIRE(s.inserted_elements() == 4);
	s.add(5);
	REQUIRE(s.inserted_elements() == 5);
	s.add(6);
	REQUIRE(s.inserted_elements() == 6);
	s.add(7);
	REQUIRE(s.inserted_elements() == 7);
	s.add(8);
	REQUIRE(s.inserted_elements() == 8);
	s.add(9);
	REQUIRE(s.inserted_elements() == 9);
	s.add(10);
	REQUIRE(s.inserted_elements() == 10);
	s.add(11);
	REQUIRE(s.inserted_elements() == 10);
}

TEST_CASE("Removing integers", "[Set<int, 10>::remove]") {
	Set <int, 10> s;
	
	s.remove(0);
	REQUIRE(s.inserted_elements() == 0);
	
	s.add(1);
	s.add(2);
	s.add(3);
	s.add(4);
	s.add(5);
	s.add(6);
	s.add(7);
	s.add(8);
	s.add(9);
	s.add(10);
	
	s.remove(10);
	REQUIRE(s.inserted_elements() == 9);
	
	s.remove(9);
	REQUIRE(s.inserted_elements() == 8);
	
	
	s.remove(9);
	REQUIRE(s.inserted_elements() == 8);
	
	s.remove(1);
	REQUIRE(s.inserted_elements() == 7);
	
	s.remove(-1);
	REQUIRE(s.inserted_elements() == 7);
}

TEST_CASE("Contains integers", "[Set<int, 10>::contains]") {
	Set <int, 10> s;
	
	s.add(10);
	
	REQUIRE(s.contains(10));
	REQUIRE(!s.contains(11));
	s.remove(10);
	REQUIRE(!s.contains(10));
	s.add(10);
	REQUIRE(s.contains(10));
}


TEST_CASE("Adding characters", "[Set<char, 5>::add]") {
	Set <char, 5> s;
	
	REQUIRE(s.inserted_elements() == 0);
	s.add('a');
	REQUIRE(s.inserted_elements() == 1);
	s.add('b');
	REQUIRE(s.inserted_elements() == 2);
	s.add('c');
	REQUIRE(s.inserted_elements() == 3);
	s.add('d');
	REQUIRE(s.inserted_elements() == 4);
	s.add('a');
	REQUIRE(s.inserted_elements() == 4);
	s.add('e');
	REQUIRE(s.inserted_elements() == 5);
	s.add('f');
	REQUIRE(s.inserted_elements() == 5);
}

TEST_CASE("Max integers", "[Set<int, 10>::max]") {
	Set <int, 10> s;
	
	s.remove(0);
	REQUIRE(s.inserted_elements() == 0);
	
	s.add(1);
	s.add(2);
	s.add(3);
	s.add(4);
	s.add(5);
	s.add(6);
	s.add(7);
	s.add(8);
	s.add(9);
	s.add(10);
	
	REQUIRE(s.max() == 10);
	
	s.remove(10);
	REQUIRE(s.max()== 9);
	
	s.remove(9);
	REQUIRE(s.max()== 8);
	
	
	s.remove(9);
	REQUIRE(s.max() == 8);
	
	s.add(1);
	REQUIRE(s.max() == 8);
	
	s.add(100);
	REQUIRE(s.max() == 100);
}

TEST_CASE("Removing characters", "[Set<char, 5>::remove]") {
	Set <char, 5> s;
	
	s.remove('a');
	REQUIRE(s.inserted_elements() == 0);
	
	s.add('a');
	s.add('b');
	s.add('c');
	s.add('d');
	s.add('e');
	
	s.remove('e');
	REQUIRE(s.inserted_elements() == 4);
	
	s.remove('d');
	REQUIRE(s.inserted_elements() == 3);
	
	
	s.remove('d');
	REQUIRE(s.inserted_elements() == 3);
	
	s.remove('c');
	REQUIRE(s.inserted_elements() == 2);
	
	s.remove('z');
	REQUIRE(s.inserted_elements() == 2);
}

TEST_CASE("Contains character", "[Set<char, 5>::contains]") {
	Set <char, 5> s;
	
	s.add('a');
	
	REQUIRE(s.contains('a'));
	REQUIRE(!s.contains('\n'));
	s.remove('a');
	REQUIRE(!s.contains('a'));
	s.add('a');
	REQUIRE(s.contains('a'));
}

TEST_CASE("Max chars", "[Set<char, 3>::max]") {
	Set <char, 3> s;
	
	REQUIRE_THROWS(s.max());
	
	s.add('a');
	REQUIRE(s.max() == 'a');
	s.add('z');
	REQUIRE(s.max() == 'z');
	s.add('b');
	REQUIRE(s.max() == 'z');
	s.remove('z');
	REQUIRE(s.max() == 'b');
	s.add('c');
	REQUIRE(s.max() == 'c');
	s.add('z');
	REQUIRE(s.max() == 'c');
}