#ifdef BMPTK_TARGET
   #include "hwlib.hpp"
   #define COUT hwlib::cout
#else
   #include <iostream>
   #define std::cout std::cerr << "\n" << std::flush
   #define COUT std::cerr
#endif
#include "rtttl_converter.hpp"

bool is_digit( char c ){ return ( c >= '0' ) && ( c <= '9' ); }
bool is_lowercase( char c ){ return ( c >= 'a' ) && ( c <= 'z' ); }
int frequencies[] = { 440'000, 493'880, 261'630, 293'660, 329'630, 349'230, 392'000 };

std::string determineNote(const int frequency) {
	switch(frequency){
		case note::A4:
			return "note::A4";
		case note::A4s:
			return "note::A4s";
		case note::B4:
			return "note::B4";
		case note::C5:
			return "note::C5";
		case note::D5:
			return "note::D5";
		case note::D5s:
			return "note::D5s";
		case note::E5:
			return "note::E5";
		case note::F5:
			return "note::F5s";
		case note::G5:
			return "note::G5";
		case note::G5s:
			return "note::G5s";
		case note::A5:
			return "note::A5";
		case note::A5s:
			return "note::A5s";
		case note::B5:
			return "note::B5";
	}
	return std::to_string(frequency);
}   

std::string determineDuration(const int duration) {
	switch(duration){
		case note::dF:
			return "note::dF";
		case note::dH:
			return "note::dH";
		case note::dQ:
			return "note::dQ";
	}
	return std::to_string(duration);
}

void rtttl_generate_melody( const char *s ){
	int def_duration = 4, def_octave = 6, beat = 100, value;
	int duration, octave, frequency;
	int state = 0;
	char def;
	bool dot;
	std::ofstream melodyfile;
	melodyfile.open ("melody.cpp", std::ios::out | std::ios::trunc);
	melodyfile << "#include \"melody.hpp\" \n\n"
				<< "void melody::play( note_player & p ){\n";

   for( const char * p = s; state >= 0; p++ ){
      const char c = *p;

      switch(state ){
             
         // title 
         case 0:
            // ignore title (chars up until the first ':')
            if( c == ':' ){
               state = 1;
            } 
            break;
               
          // defaults  
          case 1:
               // end of the defaults, start of the melody                
            if( c == ':' ){                
               state = 3;
               
            // start of a default (d,o,b)               
            } if( is_lowercase( c )){
               def = c;
               state = 2;
               
            // unrecognised default letter             
            } else {
               std::cout << "c=[" << c << "]";
            } 
            break;   

         // defaults, letter has been stored in def
         case 2:
            // start the value
            if( c == '=' ){
               value = 0;
               
            // digit: update the value
            } else if( is_digit( c )){
               value = ( 10 * value ) + ( c - '0' );
               
            // end of the value: update the default   
            } else if(( c == ':' ) || ( c == ',' )) {
               if( def == 'o'){
                  def_octave = value;
               } else if( def == 'd' ){
                  def_duration = value;
               } else if( def == 'b' ){
                  beat = value;
               } else {
                  std::cout << "def=[" << def << "]";
               }   
               state = ( c == ':' ) ? 3 : 1;
               
            // unrecognised letter in the value of a default
            } else {
               std::cout << "c=[" << c << "]";
            }
            break;
            
         // note start, set defaults
         case 3:  
            duration = def_duration;
            octave = def_octave;
            state = 4;
            dot = 0;
            
            // ignore a space before a note
            if( c == ' ' ){
                break;
            }
            
            // deliberate fallthrough!!
               
         // duration 1  
         case 4:   
            if( is_digit( c )){
               duration = c -'0';
               state = 5;
               break;
            }            
            // deliberate fallthrough!!
            
         // duration 2 
         case 5:   
            if( is_digit( c )){
               duration = ( duration * 10 ) + ( c -'0' );
               state = 6;
               break;
            }            
            // deliberate fallthrough!!
               
         // note letter   
         case 6:   
            // select the note, or a pause
            if( is_lowercase( c )){
               if( c == 'p' ){
                   frequency = 0;
               } else {
                  frequency = frequencies[ c - 'a' ]; 
               }   
               
            // unrecognised letter in note specification               
            } else {
               std::cout << "c=[" << c << "]";
            }   
            state = 7;
            break;
               
         // optional #   
         case 7:   
            if( c == '#' ){
               frequency = 10595LL * frequency / 10000;
               state = 8;
               break;
            }
            // deliberate fallthrough!!

         // optional .
         case 8:
            if( c == '.' ){
               dot = 1;
               state = 9;
               break;
            }            
            // deliberate fallthrough!!

         case 9:
            if( is_digit( c )){
               octave = c - '0';
               state = 10;
               break;
            }        
            // deliberate fallthrough!!
               
         case 10:   
            if( ( c == ',' ) || ( c == '\0') ){
               while( octave > 4 ){ --octave; frequency *= 2; }
               if( dot ){
                  duration += duration / 2;
               }
               int note_duration = ( 4 * 60'000'000 ) / ( duration * beat );

               melodyfile << "\tp.play( note{ " << determineNote(frequency/1000) << ", " << determineDuration(note_duration) <<" } );\n";
			   
               state = 3;

            // unrecognised character in or after note 
            } else {
               std::cout << "c=[" << c << "]";
            }
            if( c == '\0' ){
               state = -1;
            }
            break;            
      }         
   }

	melodyfile << "}";
	melodyfile.flush();
	melodyfile.close();
}