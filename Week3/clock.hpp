#ifndef _CLOCK_HPP_
#define _CLOCK_HPP_

#include "hwlib.hpp"
#include "math.h"
#include "clock_functions.hpp"

class Clock
{
  private:
    hwlib::window &display;
    void drawPlate();
    void drawMinute(int minute);
    void drawHour(int hour, int minute);
    pos_lookup pos_helper;
  public:
    Clock(hwlib::window &window);
    void draw(double h, double m);
};

#endif