#ifndef _CLOCK_FUNCTIONS_HPP_
#define _CLOCK_FUNCTIONS_HPP_

#define PI 3.14159265359

#include <math.h>

#include <array>
#include <utility>
#include <functional>

const static int CLOCK_DIAMETER = 50;
const static int SCREEN_HEIGHT = 64;
const static int SCREEN_WIDTH = 128;

class pos_lookup {
private:
    std::array < std::pair<int, int>, 60 > positions;
public:
    constexpr pos_lookup():
        positions{}
    {
        for (int i = 0; i < 60; i++) {
            positions[i] = std::make_pair(SCREEN_WIDTH/2 + CLOCK_DIAMETER/2 * cos(6 * i * (PI/180.0)), 
                                        SCREEN_HEIGHT/2 + CLOCK_DIAMETER/2 * sin(6 * i * (PI/180.0)));
        }
    }

    constexpr std::pair<int, int> get_pos(int degrees) const {
        return positions[degrees/6];
    }
};

#endif