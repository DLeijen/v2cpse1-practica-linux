#include "clock.hpp"

void Clock::drawPlate()
{
    hwlib::circle(hwlib::xy(display.size.x / 2, display.size.y / 2), 30).draw(display);
};

void Clock::drawMinute(int minute)
{
    minute %= 60;
    // minute arm moves 6 degrees every minutes.
    int degrees = 6 * minute;
    int x = pos_helper.get_pos(degrees).first;
    int y = pos_helper.get_pos(degrees).second;
    hwlib::line(hwlib::xy(display.size.x / 2, display.size.y / 2), hwlib::xy(x, y)).draw(display);
    hwlib::line(hwlib::xy(display.size.x / 2, display.size.y / 2), hwlib::xy(x, y)).draw(display);
};

void Clock::drawHour(int hour, int minute)
{
    hour %= 12;
    minute %= 60;
    // hour arm moves 30 degrees per hour + 6 degrees every 12 minutes.
    int degrees = 30 * hour + 6 * (minute/12);
    int x = pos_helper.get_pos(degrees).first;
    int y = pos_helper.get_pos(degrees).second;
    hwlib::line(hwlib::xy(display.size.x / 2, display.size.y / 2), hwlib::xy(x, y)).draw(display);
};

void Clock::draw(double h, double m)
{
    drawPlate();
    drawHour(h, m);
    drawMinute(m);
};

Clock::Clock(hwlib::window &window) : display(window){};