#include "hwlib.hpp"
#include "clock_functions.hpp"
#include "clock.hpp"

#include <string>

#define _SPEEDUP_FACTOR_ 60
#define _MICROSECONDS_IN_MINUTE_ 60ll * 1000000ll
#define _MICROSECONDS_IN_HOUR_ 60ll * _MICROSECONDS_IN_MINUTE_

int main( void ){
   pos_lookup pos;

   // wait for the PC console to start
   hwlib::wait_ms(2000);

   for (int degrees = 0; degrees < 360; degrees += 6) {
      hwlib::cout << degrees << ": " << pos.get_pos(degrees).first << "; " << pos.get_pos(degrees).second << ";\n";
   }

   namespace target = hwlib::target;

   auto scl = target::pin_oc(target::pins::scl);
   auto sda = target::pin_oc(target::pins::sda);

   auto i2c_bus = hwlib::i2c_bus_bit_banged_scl_sda(sda, scl);

   auto oled = hwlib::glcd_oled_i2c_128x64_fast_buffered(i2c_bus, 0x3c);

   hwlib::cout << "Running\n";

   int hour, minute;
   uint_fast64_t current_time = hwlib::now_us();

   auto clock = Clock(oled);

   while (1)
   {
      current_time = hwlib::now_us();
      // Determine current minute
      minute = (current_time / (_MICROSECONDS_IN_MINUTE_/_SPEEDUP_FACTOR_)) % 60;
      // Determine current hour
      hour = (current_time / (_MICROSECONDS_IN_HOUR_/_SPEEDUP_FACTOR_)) % 12;
      hwlib::cout << "time: " << hour << ":" << minute << "\n";
      clock.draw(hour, minute);
      oled.flush();
      oled.clear();
   } 

}
