#include "hwlib.hpp"
#include "note_player_print.hpp"

  
void note_player_print::play( const note & n ){
    hwlib::cout << n.duration << ": "<< n.frequency << ";\n"; 
}
