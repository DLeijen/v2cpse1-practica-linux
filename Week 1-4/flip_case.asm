.cpu cortex-m0

.text
.global flip_case
flip_case:
	push { lr }
	cmp r0, #'z' 		
	bgt done 			// Character is not an alphabetic character so we're done
	cmp r0, #'a' 		// if character is lowercase...
	bge to_upper 	// we need to make it uppercase
	cmp r0, #'Z' 		 
	bgt done 			// Character is not an alphabetic character so we're done
	cmp r0, #'A'		// if character is uppercase...
	bge to_lower 		// we need to make it lowercase
	b done
to_upper:
	sub r0, #('a'-'A') 	// We need to subtract to make uppercase
	b done
to_lower:
	add r0, #('a'-'A') 	// We need to add to make lowercase
	b done
done:
	pop { pc }