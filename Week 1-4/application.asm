.cpu cortex-m0

.global application

.data
str:
	.asciz "Hello world, the ANSWER is 42! @[]`{}~\n"

.text
application:
	push { lr }
	ldr r0, =str
	bl print_asciz
	pop { pc }