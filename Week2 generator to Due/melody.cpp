#include "melody.hpp" 

void melody::play( note_player & p ){
	p.play( note{ note::E5, 600000 } );
	p.play( note{ note::E5, 600000 } );
	p.play( note{ note::E5, 600000 } );
	p.play( note{ note::C5, 300000 } );
	p.play( note{ 0, 150000 } );
	p.play( note{ note::G5, 150000 } );
	p.play( note{ note::E5, 600000 } );
	p.play( note{ note::C5, 300000 } );
	p.play( note{ 0, 150000 } );
	p.play( note{ note::G5, 150000 } );
	p.play( note{ note::E5, 600000 } );
	p.play( note{ 0, 600000 } );
	p.play( note{ note::B5, 600000 } );
	p.play( note{ note::B5, 600000 } );
	p.play( note{ note::B5, 600000 } );
	p.play( note{ 1046, 300000 } );
	p.play( note{ 0, 150000 } );
	p.play( note{ note::G5, 150000 } );
	p.play( note{ note::D5s, 600000 } );
	p.play( note{ note::C5, 300000 } );
	p.play( note{ 0, 150000 } );
	p.play( note{ note::G5, 150000 } );
	p.play( note{ note::E5, 600000 } );
	p.play( note{ 0, 300000 } );
}