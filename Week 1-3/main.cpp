#include "hwlib.hpp"


extern "C" {
	void application();
	void print_asciz(const char *s);
	unsigned char flip_case(unsigned char c) {
		// Offset between capital & small letter
		unsigned char offset = 'a' - 'A';
        // c is capital letter
        if ('A' < c && c < 'Z') {
            c += offset;
        }
        // c is small letter
        else if ('a' < c && c < 'z') {
            c -= offset;
        }
        return c;
    }
	void uart_put_char(char c) {
	    hwlib::cout << c;
	}
}

int main(void) {
	namespace target = hwlib::target;

	// wait for the PC console to start
	hwlib::wait_ms(2000);

	application();
}
